/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *     Copyright (C) 2020 Leysme
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.gameplay.listener;

import de.floorisjava.acidisland.modules.islands.Islands;
import de.floorisjava.acidisland.modules.quests.Quests;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

import java.util.Objects;

public class SkeletonBlazeConversionListener implements Listener {

    /**
     * The island API
     */
    private final Islands islands;

    /**
     * The quests API
     */
    private final Quests quests;

    /**
     * Constructor
     *
     * @param islands The islands API
     * @param quests  The quests API
     */
    public SkeletonBlazeConversionListener(Islands islands, Quests quests) {
        this.islands = islands;
        this.quests = quests;
    }

    @EventHandler
    public void onEntityDeath(final EntityDeathEvent event) {
        if (event.getEntityType() == EntityType.SKELETON) {
            final Location location = event.getEntity().getLocation();
            final World world = Objects.requireNonNull(location.getWorld());
            if (islands.getIslandWorlds().contains(world)) {
                if (location.getBlock().getType() == Material.LAVA) {
                    world.spawnEntity(location, EntityType.BLAZE);

                    if (quests != null) {
                        world.getNearbyEntities(location, 32, 32, 32, Player.class::isInstance).stream()
                                .map(Player.class::cast)
                                .forEach(quests::playerWitnessedSkeletonCombustion);
                    }
                }
            }
        }
    }
}
