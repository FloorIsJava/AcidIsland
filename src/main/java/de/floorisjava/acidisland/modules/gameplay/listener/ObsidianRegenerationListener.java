/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 Leysme
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.gameplay.listener;

import de.floorisjava.acidisland.modules.islands.Islands;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockCanBuildEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Objects;

/**
 * Allow for obsidian to be converted into lava with a bucket
 */
public class ObsidianRegenerationListener implements Listener {

    /**
     * The islands API
     */
    private final Islands islands;

    /**
     * Constructor
     *
     * @param islands Islands API
     */
    public ObsidianRegenerationListener(final Islands islands) {
        this.islands = islands;
    }

    @EventHandler
    public void onPlayerInteract(final PlayerInteractEvent e) {
        // Test for valid interaction with obsidian.
        if (e.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }

        final Block clickedBlock = Objects.requireNonNull(e.getClickedBlock());
        if (clickedBlock.getType() != Material.OBSIDIAN) {
            return;
        }

        // Check if world is a island world.
        final Location location = e.getClickedBlock().getLocation();
        final World world = Objects.requireNonNull(location.getWorld());
        if (!islands.getIslandWorlds().contains(world)) {
            return;
        }

        // Test for bucket in hand.
        final Player player = e.getPlayer();
        final ItemStack inHand = player.getInventory().getItemInMainHand();
        if (inHand.getType() != Material.BUCKET || inHand.getAmount() != 1) {
            return;
        }

        // Check if the player can build at this position.
        final BlockData airBlockData = Bukkit.createBlockData(Material.AIR);
        final BlockCanBuildEvent blockCanBuildEvent = new BlockCanBuildEvent(clickedBlock, player, airBlockData, true);
        Bukkit.getPluginManager().callEvent(blockCanBuildEvent);
        if (!blockCanBuildEvent.isBuildable()) {
            return;
        }

        // Everything worked. Now set item and remove obsidian.
        player.getInventory().setItemInMainHand(new ItemStack(Material.LAVA_BUCKET));
        e.getClickedBlock().setType(Material.AIR);

        e.setCancelled(true);
    }

}
