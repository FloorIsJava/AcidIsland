/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *     Copyright (C) 2020 Leysme
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.gameplay;

import de.floorisjava.acidisland.modules.BaseModule;
import de.floorisjava.acidisland.modules.ModuleManager;
import de.floorisjava.acidisland.modules.gameplay.listener.ObsidianRegenerationListener;
import de.floorisjava.acidisland.modules.gameplay.listener.SkeletonBlazeConversionListener;
import de.floorisjava.acidisland.modules.gameplay.listener.WitchRedstoneDropListener;
import de.floorisjava.acidisland.modules.islands.Islands;
import de.floorisjava.acidisland.modules.quests.Quests;

import java.util.Objects;
import java.util.Random;

/**
 * Modifies vanilla gameplay to better suit acid island.
 */
public class GameplayModule extends BaseModule {

    /**
     * A random number generator.
     */
    private static final Random RANDOM = new Random();

    /**
     * The quest module API.
     */
    private Quests quests;

    /**
     * The island module API.
     */
    private Islands islands;

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public GameplayModule(final ModuleManager moduleManager) {
        super(moduleManager);

        addListener(() -> new SkeletonBlazeConversionListener(islands, quests));
        addListener(() -> new WitchRedstoneDropListener(islands));
        addListener(() -> new ObsidianRegenerationListener(islands));
    }

    @Override
    public void startModule() {
        quests = getModuleManager().get(Quests.class);
        islands = Objects.requireNonNull(getModuleManager().get(Islands.class));
    }


}
