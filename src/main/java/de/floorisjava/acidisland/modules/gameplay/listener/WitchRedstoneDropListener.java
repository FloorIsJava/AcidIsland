/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *     Copyright (C) 2020 Leysme
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.gameplay.listener;

import de.floorisjava.acidisland.modules.islands.Islands;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Objects;
import java.util.Random;

/**
 * This class will modify the drops of witches to allow redstone drops
 */
public class WitchRedstoneDropListener implements Listener {

    /**
     * A random number generator
     */
    private static final Random RANDOM = new Random();

    /**
     * The Island API
     */
    private final Islands islands;

    /**
     * Constructor
     *
     * @param islands The islands api
     */
    public WitchRedstoneDropListener(Islands islands) {
        this.islands = islands;
    }

    @EventHandler
    public void onEntityDeath(EntityDeathEvent event) {
        if (event.getEntityType() == EntityType.WITCH) {
            final Location location = event.getEntity().getLocation();
            final World world = Objects.requireNonNull(location.getWorld());
            if (islands.getIslandWorlds().contains(world)) {
                if (RANDOM.nextInt(100) == 0) {
                    event.getDrops().add(new ItemStack(Material.NETHER_WART));
                }
            }
        }
    }
}
