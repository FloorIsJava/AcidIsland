/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.deathpenalty;

import de.floorisjava.acidisland.modules.BaseModule;
import de.floorisjava.acidisland.modules.ModuleManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Increases penalties on death.
 */
public class DeathPenaltyModule extends BaseModule implements Listener {

    /**
     * Storage for food levels at time of death.
     */
    private final Map<UUID, Integer> deathFoodLevels = new HashMap<>();

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public DeathPenaltyModule(final ModuleManager moduleManager) {
        super(moduleManager);

        addListener(() -> this);
    }

    @EventHandler
    public void onPlayerDeath(final PlayerDeathEvent event) {
        deathFoodLevels.put(event.getEntity().getUniqueId(), event.getEntity().getFoodLevel());
    }

    @EventHandler
    public void onPlayerRespawn(final PlayerRespawnEvent event) {
        final UUID uuid = event.getPlayer().getUniqueId();
        if (deathFoodLevels.containsKey(uuid)) {
            final Player player = event.getPlayer();
            final int level = deathFoodLevels.get(uuid);
            Bukkit.getScheduler().scheduleSyncDelayedTask(getModuleManager(), () -> {
                player.setFoodLevel(Math.max(6, level));
                player.setHealth(Math.max(10, level));
            }, 1L);
        }
    }
}
