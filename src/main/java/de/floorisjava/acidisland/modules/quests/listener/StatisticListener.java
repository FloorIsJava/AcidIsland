/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.quests.listener;

import de.floorisjava.acidisland.modules.quests.Quests;
import lombok.RequiredArgsConstructor;
import org.bukkit.Material;
import org.bukkit.Statistic;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerStatisticIncrementEvent;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Listens to statistics changes.
 */
@RequiredArgsConstructor
public class StatisticListener implements Listener {

    /**
     * The mining advancements that are granted.
     */
    private static final Map<Material, Map<Integer, String>> mineAdvancements = new HashMap<>();

    static {
        getMaterialAdvancements(Material.COBBLESTONE, true).put(2500, "vanity/tons_of_cobble");
        getMaterialAdvancements(Material.COBBLESTONE, true).put(10000, "vanity/tons_of_cobble_2");
        getMaterialAdvancements(Material.COBBLESTONE, true).put(50000, "vanity/tons_of_cobble_3");
    }

    /**
     * The API for quests.
     */
    private final Quests quests;

    /**
     * Obtains the material advancement map.
     *
     * @param material The material.
     * @param create   {@code true} if a map shall be created if there is none.
     * @return The material advancement map.
     */
    private static Map<Integer, String> getMaterialAdvancements(final Material material, final boolean create) {
        if (create) {
            return mineAdvancements.computeIfAbsent(material, m -> new HashMap<>());
        } else {
            return mineAdvancements.getOrDefault(material, Collections.emptyMap());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerStatisticIncrement(final PlayerStatisticIncrementEvent event) {
        if (event.getStatistic() == Statistic.CHEST_OPENED) {
            quests.awardAdvancement(event.getPlayer(), "mainline/take_inventory");
        } else if (event.getStatistic() == Statistic.MINE_BLOCK) {
            if (event.getMaterial() != null) {
                final String advancementKey =
                        getMaterialAdvancements(event.getMaterial(), false).get(event.getNewValue());
                if (advancementKey != null) {
                    quests.awardAdvancement(event.getPlayer(), advancementKey);
                }
            }
        }
    }
}
