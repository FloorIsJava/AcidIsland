/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.quests.listener;

import de.floorisjava.acidisland.modules.quests.Quests;
import lombok.RequiredArgsConstructor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerEditBookEvent;

/**
 * Listens to book edits.
 */
@RequiredArgsConstructor
public class BookEditListener implements Listener {

    /**
     * The API for quests.
     */
    private final Quests quests;

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerEditBook(final PlayerEditBookEvent event) {
        quests.awardAdvancement(event.getPlayer(), "mainline/write_book");
    }
}
