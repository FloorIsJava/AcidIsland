/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.quests;

import de.floorisjava.acidisland.modules.BaseModule;
import de.floorisjava.acidisland.modules.ModuleManager;
import de.floorisjava.acidisland.modules.quests.listener.BookEditListener;
import de.floorisjava.acidisland.modules.quests.listener.StatisticListener;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.advancement.Advancement;
import org.bukkit.advancement.AdvancementProgress;
import org.bukkit.entity.Player;

/**
 * Provides challenges and quests.
 */
public class QuestModule extends BaseModule implements Quests {

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public QuestModule(final ModuleManager moduleManager) {
        super(moduleManager);

        addListener(() -> new StatisticListener(this));
        addListener(() -> new BookEditListener(this));
    }

    @Override
    public void playerJoinedIsland(final Player player) {
        awardAdvancement(player, "mainline/create_island");
    }

    @Override
    public void playerReceivedAcidRainDamage(final Player player) {
        awardAdvancement(player, "mainline/rain_hurts");
    }

    @Override
    public void playerWitnessedSkeletonCombustion(final Player player) {
        awardAdvancement(player, "hunting/skeleton_witch_trial");
    }

    /**
     * Awards an advancement.
     *
     * @param player The player.
     * @param key    The key.
     */
    @Override
    public void awardAdvancement(final Player player, final String key) {
        final Advancement advancement = Bukkit.getAdvancement(new NamespacedKey(getModuleManager(), key));
        if (advancement != null) {
            awardAdvancement(player.getAdvancementProgress(advancement));
        } else {
            logger().warning("Can't find advancement " + key);
        }
    }

    /**
     * Awards an advancement.
     *
     * @param progress The advancement progress.
     */
    private void awardAdvancement(final AdvancementProgress progress) {
        if (!progress.isDone()) {
            progress.getRemainingCriteria().forEach(progress::awardCriteria);
        }
    }
}
