/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.islands.populator;

import org.bukkit.Location;

/**
 * Populates islands.
 */
public interface IslandPopulator {

    /**
     * Populates an island at the given location. The location is the lowest block of the center of the island
     * protection domain.
     *
     * @param baseLocation The base location.
     */
    void populate(Location baseLocation);

    /**
     * Obtains the spawn location from the base location.
     *
     * @param baseLocation The base location.
     * @return The spawn location.
     */
    Location getSpawn(Location baseLocation);
}
