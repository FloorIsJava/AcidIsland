/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.islands.listener;

import de.floorisjava.acidisland.modules.islands.model.IslandManager;
import de.floorisjava.acidisland.modules.islands.model.MultiworldIslandManager;
import lombok.RequiredArgsConstructor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.entity.AreaEffectCloud;
import org.bukkit.entity.Boat;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Mob;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.Snowman;
import org.bukkit.entity.Tameable;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockCanBuildEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.block.EntityBlockFormEvent;
import org.bukkit.event.block.SpongeAbsorbEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityTameEvent;
import org.bukkit.event.entity.LingeringPotionSplashEvent;
import org.bukkit.event.entity.PlayerLeashEntityEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerShearEntityEvent;
import org.bukkit.event.player.PlayerTakeLecternBookEvent;
import org.bukkit.event.player.PlayerUnleashEntityEvent;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.event.world.StructureGrowEvent;

import java.util.Optional;
import java.util.function.Function;
import java.util.logging.Logger;

/**
 * Takes care of protecting islands from manipulation.
 */
@RequiredArgsConstructor
public class ProtectionListener implements Listener {

    /**
     * A logger.
     */
    private final Logger logger;

    /**
     * The multi-world island manager.
     */
    private final MultiworldIslandManager manager;

    @EventHandler
    public void onBlockBreak(final BlockBreakEvent event) {
        cancelDisallowedEvents(event, BlockBreakEvent::getPlayer, BlockBreakEvent::getBlock);
    }

    @EventHandler
    public void onBlockBurn(final BlockBurnEvent event) {
        if (event.getIgnitingBlock() == null) {
            event.setCancelled(true);
        } else {
            cancelDisallowedEvents(event, BlockBurnEvent::getIgnitingBlock, BlockBurnEvent::getBlock);
        }
    }

    @EventHandler
    public void onBlockCanBuild(final BlockCanBuildEvent event) {
        if (!isActionAllowed(event.getPlayer(), event.getBlock())) {
            event.setBuildable(false);
        }
    }

    @EventHandler
    public void onBlockDamage(final BlockDamageEvent event) {
        cancelDisallowedEvents(event, BlockDamageEvent::getPlayer, BlockDamageEvent::getBlock);
    }

    @EventHandler
    public void onBlockExplode(final BlockExplodeEvent event) {
        event.blockList().removeIf(x -> !isActionAllowed(event.getBlock(), x));
    }

    @EventHandler
    public void onBlockFromTo(final BlockFromToEvent event) {
        cancelDisallowedEvents(event, BlockFromToEvent::getBlock, BlockFromToEvent::getToBlock);
    }

    @EventHandler
    public void onBlockIgnite(final BlockIgniteEvent event) {
        if (event.getIgnitingBlock() != null) {
            cancelDisallowedEvents(event, BlockIgniteEvent::getIgnitingBlock, BlockIgniteEvent::getBlock);
        } else if (event.getIgnitingEntity() != null) {
            cancelDisallowedEvents(event, BlockIgniteEvent::getIgnitingEntity, BlockIgniteEvent::getBlock);
        }
    }

    @EventHandler
    public void onBlockPistonExtend(final BlockPistonExtendEvent event) {
        if (event.getBlocks().stream()
                .anyMatch(b ->
                        !isActionAllowed(event.getBlock(), b)
                        || !isActionAllowed(event.getBlock(), b.getRelative(event.getDirection())))) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockPistonRetract(final BlockPistonRetractEvent event) {
        if (event.getBlocks().stream()
                .anyMatch(b ->
                        !isActionAllowed(event.getBlock(), b)
                        || !isActionAllowed(event.getBlock(), b.getRelative(event.getDirection())))) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockPlace(final BlockPlaceEvent event) {
        cancelDisallowedEvents(event, BlockPlaceEvent::getPlayer, BlockPlaceEvent::getBlock);
    }

    @EventHandler
    public void onBlockSpread(final BlockSpreadEvent event) {
        cancelDisallowedEvents(event, BlockSpreadEvent::getSource, BlockSpreadEvent::getBlock);
    }

    @EventHandler
    public void onEntityBlockForm(final EntityBlockFormEvent event) {
        // Override: Snowmans can create snow
        if (event.getEntity() instanceof Snowman) {
            return;
        }

        cancelDisallowedEvents(event, EntityBlockFormEvent::getEntity, EntityBlockFormEvent::getBlock);
    }

    @EventHandler
    public void onSpongeAbsorb(final SpongeAbsorbEvent event) {
        event.getBlocks().removeIf(x -> !isActionAllowed(event.getBlock(), x));
    }

    @EventHandler
    public void onEntityChangeBlockEvent(final EntityChangeBlockEvent event) {
        // Override: Sheep can change grass.
        if (event.getEntity() instanceof Sheep) {
            return;
        }

        // Override: Allow falling blocks.
        if (event.getEntity() instanceof FallingBlock) {
            return;
        }

        cancelDisallowedEvents(event, EntityChangeBlockEvent::getEntity, EntityChangeBlockEvent::getBlock);
    }

    @EventHandler
    public void onEntityDamageByEntity(final EntityDamageByEntityEvent event) {
        if (isTypeProtected(event.getEntityType())) {
            // Override: Area Effect Clouds that are spawned have been created priviledgedly.
            if (event.getDamager() instanceof AreaEffectCloud) {
                return;
            }

            cancelDisallowedEvents(event, EntityDamageByEntityEvent::getDamager, EntityDamageByEntityEvent::getEntity);
        }
    }

    @EventHandler
    public void onEntityExplode(final EntityExplodeEvent event) {
        event.blockList().removeIf(x -> !isActionAllowed(event.getEntity(), x));
    }

    @EventHandler
    public void onEntityTame(final EntityTameEvent event) {
        cancelDisallowedEvents(event, EntityTameEvent::getOwner, EntityTameEvent::getEntity);
    }

    @EventHandler
    public void onLingeringPotionSplash(final LingeringPotionSplashEvent event) {
        cancelDisallowedEvents(event, LingeringPotionSplashEvent::getEntity, LingeringPotionSplashEvent::getEntity);
    }

    @EventHandler
    public void onPlayerLeashEntity(final PlayerLeashEntityEvent event) {
        cancelDisallowedEvents(event, PlayerLeashEntityEvent::getPlayer, PlayerLeashEntityEvent::getEntity);
    }

    @EventHandler
    public void onPotionSplash(final PotionSplashEvent event) {
        event.getAffectedEntities().forEach(entity -> {
            if (isTypeProtected(entity.getType())) {
                if (!isActionAllowed(event.getPotion(), entity)) {
                    event.setIntensity(entity, 0.0);
                }
            }
        });
    }

    @EventHandler
    public void onHangingBreakByEntity(final HangingBreakByEntityEvent event) {
        cancelDisallowedEvents(event, HangingBreakByEntityEvent::getRemover, HangingBreakByEntityEvent::getEntity);
    }

    @EventHandler
    public void onHangingBreak(final HangingBreakEvent event) {
        switch (event.getCause()) {
            case DEFAULT:
            case EXPLOSION:
                event.setCancelled(true);
                break;
            default:
                break;
        }
    }

    @EventHandler
    public void onHangingPlace(final HangingPlaceEvent event) {
        cancelDisallowedEvents(event, HangingPlaceEvent::getPlayer, HangingPlaceEvent::getEntity);
    }

    @EventHandler
    public void onPlayerBucketEmpty(final PlayerBucketEmptyEvent event) {
        cancelDisallowedEvents(event, PlayerBucketEmptyEvent::getPlayer, PlayerBucketEmptyEvent::getBlock);
    }

    @EventHandler
    public void onPlayerBucketFill(final PlayerBucketFillEvent event) {
        cancelDisallowedEvents(event, PlayerBucketFillEvent::getPlayer, PlayerBucketFillEvent::getBlock);
    }

    @EventHandler
    public void onPlayerFish(final PlayerFishEvent event) {
        if (event.getState() == PlayerFishEvent.State.CAUGHT_ENTITY && event.getCaught() != null) {
            cancelDisallowedEvents(event, PlayerFishEvent::getPlayer, PlayerFishEvent::getCaught);
        }
    }

    @EventHandler
    public void onPlayerInteractEntity(final PlayerInteractEntityEvent event) {
        if (isTypeProtected(event.getRightClicked().getType())) {
            cancelDisallowedEvents(event, PlayerInteractEntityEvent::getPlayer,
                    PlayerInteractEntityEvent::getRightClicked);
        }
    }

    @EventHandler
    public void onPlayerInteract(final PlayerInteractEvent event) {
        final Block clicked = event.getClickedBlock();
        if (clicked != null) {
            switch (event.getAction()) {
                case RIGHT_CLICK_BLOCK:
                case PHYSICAL:
                    if (isMaterialProtected(clicked.getType())) {
                        cancelDisallowedEvents(event, PlayerInteractEvent::getPlayer,
                                PlayerInteractEvent::getClickedBlock);
                    }
                    break;
            }
        }
    }

    @EventHandler
    public void onPlayerShearEntity(final PlayerShearEntityEvent event) {
        cancelDisallowedEvents(event, PlayerShearEntityEvent::getPlayer, PlayerShearEntityEvent::getEntity);
    }

    @EventHandler
    public void onPlayerTakeLecternBook(final PlayerTakeLecternBookEvent event) {
        cancelDisallowedEvents(event, PlayerTakeLecternBookEvent::getPlayer, PlayerTakeLecternBookEvent::getLectern);
    }

    @EventHandler
    public void onPlayerUnleashEntity(final PlayerUnleashEntityEvent event) {
        cancelDisallowedEvents(event, PlayerUnleashEntityEvent::getPlayer, PlayerUnleashEntityEvent::getEntity);
    }

    @EventHandler
    public void onVehicleDamage(final VehicleDamageEvent event) {
        // Override: Allow boats.
        if (event.getVehicle() instanceof Boat) {
            return;
        }

        cancelDisallowedEvents(event, VehicleDamageEvent::getAttacker, VehicleDamageEvent::getVehicle);
    }

    @EventHandler
    public void onVehicleDestroy(final VehicleDestroyEvent event) {
        // Override: Allow boats.
        if (event.getVehicle() instanceof Boat) {
            return;
        }

        cancelDisallowedEvents(event, VehicleDestroyEvent::getAttacker, VehicleDestroyEvent::getVehicle);
    }

    @EventHandler
    public void onStructureGrow(final StructureGrowEvent event) {
        event.getBlocks().removeIf(x -> !isActionAllowed(event.getLocation(), x));
    }

    /**
     * Cancels the given event if and only if the action is not allowed.
     *
     * @param event           The event.
     * @param sourceExtractor The source object extractor.
     * @param targetExtractor The target object extractor.
     * @param <T>             The event type.
     */
    private <T extends Cancellable> void cancelDisallowedEvents(final T event,
                                                                final Function<T, Object> sourceExtractor,
                                                                final Function<T, Object> targetExtractor) {
        if (!isActionAllowed(sourceExtractor.apply(event), targetExtractor.apply(event))) {
            event.setCancelled(true);
        }
    }

    /**
     * Checks whether an action originating from a source influencing a target is allowed.
     *
     * @param source The source.
     * @param target The target.
     * @return {@code true} if the action is allowed.
     */
    private boolean isActionAllowed(final Object source, final Object target) {
        // Determine the target location.
        final Location targetLocation;
        if (target instanceof Location) {
            targetLocation = (Location) target;
        } else if (target instanceof Entity) {
            targetLocation = ((Entity) target).getLocation();
        } else if (target instanceof Block) {
            targetLocation = ((Block) target).getLocation();
        } else if (target instanceof BlockState) {
            targetLocation = ((BlockState) target).getLocation();
        } else {
            if (target != null) {
                logger.warning("Unknown action target type: " + target.getClass().getSimpleName());
            } else {
                logger.warning("Null target!");
            }
            return true;
        }

        // Check that the location is associated with a world.
        final World targetWorld = targetLocation.getWorld();
        if (targetWorld == null) {
            logger.warning("Action with source " + source + " and target " + target + " has no world at target!");
            return true;
        }

        // Determine the island manager for the target location's world.
        final Optional<IslandManager> islandManagerOpt = manager.getIslandManager(targetWorld);
        if (!islandManagerOpt.isPresent()) {
            // Not an island world.
            return true;
        }
        final IslandManager islandManager = islandManagerOpt.get();

        // Determine a source player (if possible) and a source location (if needed).
        final Player sourcePlayer;
        final Location sourceLocation;
        if (source instanceof Location) {
            // Entity-agnostic action.
            sourceLocation = (Location) source;
            sourcePlayer = null;
        } else if (source instanceof Block) {
            // Entity-agnostic action.
            sourceLocation = ((Block) source).getLocation();
            sourcePlayer = null;
        } else if (source instanceof BlockState) {
            // Entity-agnostic action.
            sourceLocation = ((BlockState) source).getLocation();
            sourcePlayer = null;
        } else if (source instanceof Entity) {
            final Object issuer;
            if (source instanceof Player) {
                // Direct action by player.
                issuer = source;
            } else if (source instanceof Mob) {
                final AnimalTamer owner;
                if (source instanceof Tameable) {
                    // Possibly: Indirect action by pet.
                    owner = ((Tameable) source).getOwner();
                } else {
                    owner = null;
                }

                if (owner instanceof Player) {
                    issuer = owner;
                } else {
                    // Possibly: Indirect action by predator.
                    issuer = ((Mob) source).getTarget();
                }
            } else if (source instanceof Projectile) {
                // Possibly: Indirect action by projectile.
                issuer = ((Projectile) source).getShooter();
            } else {
                issuer = null;
            }

            if (issuer instanceof Player) {
                sourcePlayer = (Player) issuer;
                sourceLocation = null;
            } else {
                return false;
            }
        } else {
            if (source != null) {
                logger.warning("Unknown action source type: " + target.getClass().getSimpleName());
            } else {
                logger.warning("Null source!");
            }
            return false;
        }

        if (sourcePlayer != null) {
            // Check that player responsible for source has permission.
            return sourcePlayer.isOp()
                   || islandManager.getIslandAt(targetLocation)
                           .map(i -> i.canBuild(sourcePlayer.getUniqueId()))
                           .orElse(false);
        } else if (sourceLocation.getWorld() == targetWorld) {
            // Check that entity-agnostic action does not exceed protection domain.
            return islandManager.getIslandNumberAt(targetLocation) == islandManager.getIslandNumberAt(sourceLocation);
        } else {
            logger.warning("Source/Target world mismatch: " + target + " -> " + source);
            return false;
        }
    }

    /**
     * Checks whether the given entity type is protected.
     *
     * @param type The type.
     * @return {@code true} if the type is protected.
     */
    private boolean isTypeProtected(final EntityType type) {
        switch (type) {
            case ELDER_GUARDIAN:
            case WITHER_SKELETON:
            case STRAY:
            case FIREBALL:
            case HUSK:
            case SHULKER_BULLET:
            case ZOMBIE_VILLAGER:
            case EVOKER:
            case VEX:
            case VINDICATOR:
            case ILLUSIONER:
            case CREEPER:
            case SKELETON:
            case SPIDER:
            case GIANT:
            case ZOMBIE:
            case SLIME:
            case GHAST:
            case PIG_ZOMBIE:
            case ENDERMAN:
            case CAVE_SPIDER:
            case SILVERFISH:
            case BLAZE:
            case MAGMA_CUBE:
            case ENDER_DRAGON:
            case WITHER:
            case WITCH:
            case ENDERMITE:
            case GUARDIAN:
            case SHULKER:
            case PHANTOM:
            case DROWNED:
            case PILLAGER:
            case RAVAGER:
            case PLAYER:
                return false;
            default:
                return true;
        }
    }

    /**
     * Checks whether the given material is protected.
     *
     * @param material The material.
     * @return {@code true} if the material is protected.
     */
    private boolean isMaterialProtected(final Material material) {
        switch (material) {
            case CRAFTING_TABLE:
            case ENCHANTING_TABLE:
            case ENDER_CHEST:
            case LOOM:
            case CARTOGRAPHY_TABLE:
            case GRINDSTONE:
            case STONECUTTER:
            case BELL:
            case CAMPFIRE:
            case ACACIA_PRESSURE_PLATE:
            case BIRCH_PRESSURE_PLATE:
            case DARK_OAK_PRESSURE_PLATE:
            case JUNGLE_PRESSURE_PLATE:
            case OAK_PRESSURE_PLATE:
            case SPRUCE_PRESSURE_PLATE:
            case BIRCH_BUTTON:
            case ACACIA_BUTTON:
            case DARK_OAK_BUTTON:
            case JUNGLE_BUTTON:
            case OAK_BUTTON:
            case SPRUCE_BUTTON:
            case TRIPWIRE:
            case TRIPWIRE_HOOK:
                return false;
            default:
                return true;
        }
    }
}
