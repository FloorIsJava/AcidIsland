/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.islands.model;

import de.floorisjava.acidisland.modules.islands.populator.IslandPopulator;
import de.floorisjava.acidisland.modules.islands.util.IslandMath;
import de.floorisjava.acidisland.util.EntryPoint;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Represents an island.
 */
@SerializableAs("de.floorisjava.acidisland.serial.Island")
@RequiredArgsConstructor
public class Island implements ConfigurationSerializable {

    /**
     * The number of the island.
     */
    @Getter
    private final int number;

    /**
     * The owner of the island. May be {@code null}.
     */
    @Getter
    @Setter
    private UUID owner;

    /**
     * The members of the island.
     */
    private final Set<UUID> members = new HashSet<>();

    /**
     * The spawn point of the island.
     */
    @Getter
    @Setter
    private Location spawnPoint;

    /**
     * Deserialization constructor.
     *
     * @param serial The serialized object.
     */
    @EntryPoint
    public Island(final Map<String, Object> serial) {
        number = (int) serial.get("n");
        owner = Optional.ofNullable((String) serial.get("owner")).map(UUID::fromString).orElse(null);
        spawnPoint = (Location) serial.get("spawn");

        final List<?> members = (List<?>) serial.getOrDefault("members", Collections.emptyList());
        members.stream()
                .filter(String.class::isInstance)
                .map(String.class::cast)
                .map(UUID::fromString)
                .forEach(this.members::add);
    }

    /**
     * Checks whether the entity designated by the given UUID can build here.
     *
     * @param who The entity's UUID.
     * @return {@code true} if and only if the entity can build on this island.
     */
    public boolean canBuild(final UUID who) {
        return owner != null && owner.equals(who) || members.contains(who);
    }

    /**
     * Populates the island.
     *
     * @param world     The world.
     * @param manager   The island manager.
     * @param populator The populator to use.
     */
    public void populate(final World world, final IslandManager manager, final IslandPopulator populator) {
        if (!manager.hasIsland(this)) {
            throw new IllegalArgumentException("invalid manager: does not own island");
        }

        final int islandX = IslandMath.islandX(number);
        final int islandZ = IslandMath.islandZ(number);
        final Location minLocation = new Location(world, IslandMath.gridToWorld(islandX, manager.getDimensionX()), 0,
                IslandMath.gridToWorld(islandZ, manager.getDimensionZ()));

        final Location baseLocation = minLocation.add(manager.getDimensionX() / 2.0, 0, manager.getDimensionZ() / 2.0);
        populator.populate(baseLocation.clone());
        spawnPoint = populator.getSpawn(baseLocation.clone());
    }

    /**
     * Obtains the members of the island.
     *
     * @return The members of the island.
     */
    public Collection<UUID> getMembers() {
        return Collections.unmodifiableCollection(members);
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = new HashMap<>();
        result.put("n", number);
        if (spawnPoint != null) {
            result.put("spawn", spawnPoint);
        }
        if (owner != null) {
            result.put("owner", owner.toString());
        }
        if (members.size() > 0) {
            result.put("members", members.stream()
                    .map(UUID::toString)
                    .collect(Collectors.toList()));
        }
        return result;
    }
}
