/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.islands.model;

import de.floorisjava.acidisland.config.ConfigurationFile;
import lombok.RequiredArgsConstructor;
import org.bukkit.World;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Provides islands for multiple worlds.
 */
@RequiredArgsConstructor
public class MultiworldIslandManager {

    /**
     * The per-world managers.
     */
    private final Map<String, IslandManager> worldManagers = new HashMap<>();

    /**
     * The island manager storage.
     */
    private final ConfigurationFile managerStorage;

    /**
     * Obtains the island manager for the given world.
     *
     * @param world The world.
     * @return The island manager.
     */
    public Optional<IslandManager> getIslandManager(final World world) {
        return Optional.ofNullable(worldManagers.get(world.getName()));
    }

    /**
     * Creates an island manager for the given world.
     *
     * @param world      The world.
     * @param dimensionX The x dimensions of islands in that world.
     * @param dimensionZ The z dimensions of islands in that world.
     */
    public void createIslandManager(final World world, final int dimensionX, final int dimensionZ) {
        if (worldManagers.containsKey(world.getName())) {
            throw new IllegalArgumentException("world already has a manager");
        }

        final IslandManager manager;
        if (managerStorage.getConfig().contains(world.getName())) {
            manager = managerStorage.getConfig().getSerializable(world.getName(), IslandManager.class);
        } else {
            manager = new IslandManager(dimensionX, dimensionZ);
        }
        worldManagers.put(world.getName(), manager);
    }

    /**
     * Removes the island manager for the given world.
     *
     * @param world The world.
     */
    public void removeIslandManager(final World world) {
        worldManagers.remove(world.getName());
    }

    /**
     * Removes all island managers.
     */
    public void removeAllIslandManagers() {
        worldManagers.clear();
    }

    /**
     * Saves all island managers.
     */
    public void save() {
        worldManagers.forEach(managerStorage.getConfig()::set);
        managerStorage.save();
    }
}
