/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.islands.populator;

import org.bukkit.Chunk;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.type.Chest;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.Random;

/**
 * Populates a paradise island.
 */
public class ParadisePopulator implements IslandPopulator {

    /**
     * A random number generator.
     */
    private static final Random RANDOM = new Random();

    @Override
    public void populate(final Location baseLocation) {
        // Island terrain
        for (int x = -3; x <= 3; ++x) {
            for (int z = -3; z <= 3; ++z) {
                final int taxicab = Math.abs(x) + Math.abs(z);

                // Spare the corners
                if (taxicab < 6) {
                    fillColumn(baseLocation.clone().add(x, 0, z), 60 + Math.min(3, taxicab));
                }
            }
        }

        // Tree
        populateTree(baseLocation.clone().add(0, 64, 0));

        // Chest
        populateChest(baseLocation.clone().add(0, 64, -1));
    }

    @Override
    public Location getSpawn(final Location baseLocation) {
        return new Location(baseLocation.getWorld(), baseLocation.getBlockX() + 0.5, 64,
                baseLocation.getBlockZ() + 1.5);
    }

    /**
     * Populates a tree at the given base location.
     *
     * @param base The base location.
     */
    private void populateTree(final Location base) {
        Block cursor = base.getBlock();
        for (int y = 0; y < 6; ++y) {
            cursor.setType(Material.ACACIA_LOG);
            cursor = cursor.getRelative(BlockFace.UP);
        }
        cursor = cursor.getRelative(BlockFace.DOWN).getRelative(BlockFace.EAST);
        cursor.setType(Material.ACACIA_LOG);
        cursor = cursor.getRelative(BlockFace.UP).getRelative(BlockFace.EAST);
        cursor.setType(Material.ACACIA_LOG);
        final Block cursorLeavesSmall = cursor;
        cursor = cursor.getRelative(BlockFace.WEST, 3);
        cursor.setType(Material.ACACIA_LOG);
        cursor = cursor.getRelative(BlockFace.UP);
        cursor.setType(Material.ACACIA_LOG);
        final Block cursorLeavesLarge = cursor;

        populateLeaves(cursorLeavesSmall, 2, 3);
        populateLeaves(cursorLeavesSmall.getRelative(BlockFace.UP), 1, 2);

        populateLeaves(cursorLeavesLarge, 3, 5);
        populateLeaves(cursorLeavesLarge.getRelative(BlockFace.UP), 2, 2);
    }

    /**
     * Fills the column with a dirt break at the given height
     *
     * @param column     Any location in the column.
     * @param dirtBreakY The dirt break height, inclusive.
     */
    private void fillColumn(final Location column, final int dirtBreakY) {
        final Chunk chunk = column.getChunk();
        final int x = column.getBlockX() - chunk.getX() * 16;
        final int z = column.getBlockZ() - chunk.getZ() * 16;

        for (int y = 0; y < 64; ++y) {
            final Material material;
            if (y < 32) {
                material = Material.SANDSTONE;
            } else if (y < dirtBreakY) {
                material = Material.SAND;
            } else if (y < 63) {
                material = Material.DIRT;
            } else {
                material = Material.GRASS_BLOCK;
            }
            chunk.getBlock(x, y, z).setType(material);
        }
    }

    /**
     * Populates leaves.
     *
     * @param source     The source block.
     * @param radius     The radius.
     * @param maxTaxicab The max taxicab distance.
     */
    private void populateLeaves(final Block source, final int radius, final int maxTaxicab) {
        for (int x = -radius; x <= radius; ++x) {
            for (int z = -radius; z <= radius; ++z) {
                if (Math.abs(x) + Math.abs(z) <= maxTaxicab) {
                    final Block block = source.getRelative(x, 0, z);
                    if (block.getType().isAir()) {
                        block.setType(Material.ACACIA_LEAVES);
                    }
                }
            }
        }
    }

    /**
     * Populates a starting chest.
     *
     * @param location The location.
     */
    private void populateChest(final Location location) {
        final Block block = location.getBlock();
        block.setType(Material.CHEST);
        ((Chest) block.getBlockData()).setFacing(BlockFace.NORTH);

        final Inventory inventory = ((org.bukkit.block.Chest) block.getState()).getBlockInventory();
        inventory.clear();
        inventory.addItem(
                new ItemStack(Material.TORCH),
                new ItemStack(Material.BROWN_MUSHROOM),
                new ItemStack(Material.RED_MUSHROOM),
                new ItemStack(Material.CACTUS),
                new ItemStack(Material.VINE),
                new ItemStack(Material.SUGAR_CANE),
                new ItemStack(Material.KELP),
                new ItemStack(Material.BAMBOO),
                new ItemStack(Material.LAVA_BUCKET),
                new ItemStack(Material.BONE),
                new ItemStack(Material.PUMPKIN_SEEDS),
                new ItemStack(Material.MELON_SLICE),
                new ItemStack(Material.COCOA_BEANS),
                new ItemStack(Material.SWEET_BERRIES),
                new ItemStack(Material.OAK_SIGN),
                new ItemStack(Material.WRITABLE_BOOK),
                randomizeArmorColor(Material.LEATHER_HELMET),
                randomizeArmorColor(Material.LEATHER_CHESTPLATE),
                randomizeArmorColor(Material.LEATHER_LEGGINGS),
                randomizeArmorColor(Material.LEATHER_BOOTS)
        );
    }

    /**
     * Randomizes the color of a leather armor item.
     *
     * @param type The item type, must be leather armor.
     * @return The randomized armor piece.
     */
    private ItemStack randomizeArmorColor(final Material type) {
        final ItemStack item = new ItemStack(type);
        final LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
        if (meta != null) {
            final int red = Math.min(16 * RANDOM.nextInt(17), 255);
            final int green = Math.min(16 * RANDOM.nextInt(17), 255);
            final int blue = Math.min(16 * RANDOM.nextInt(17), 255);
            meta.setColor(Color.fromRGB(red, green, blue));
        }
        item.setItemMeta(meta);
        return item;
    }
}
