/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.islands.model;

import de.floorisjava.acidisland.modules.islands.util.IslandMath;
import de.floorisjava.acidisland.util.EntryPoint;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Manages all existing {@link Island}s in a world..
 */
@SerializableAs("de.floorisjava.acidisland.serial.IslandManager")
@RequiredArgsConstructor
public class IslandManager implements ConfigurationSerializable {

    /**
     * The x dimension of islands managed by this manager.
     */
    @Getter
    private final int dimensionX;

    /**
     * The z dimension of islands managed by this manager.
     */
    @Getter
    private final int dimensionZ;

    /**
     * A map that maps island number to island object.
     */
    private final Map<Integer, Island> islands = new HashMap<>();

    /**
     * Deserialization constructor.
     *
     * @param serial The serialized object.
     */
    @EntryPoint
    public IslandManager(final Map<String, Object> serial) {
        dimensionX = (int) serial.get("dimX");
        dimensionZ = (int) serial.get("dimZ");

        final List<?> islands = (List<?>) serial.get("islands");
        islands.stream()
                .filter(Island.class::isInstance)
                .map(Island.class::cast)
                .forEach(i -> this.islands.put(i.getNumber(), i));
    }

    /**
     * Obtains the island at the given location, if it exists.
     *
     * @param location The location.
     * @return The island.
     */
    public Optional<Island> getIslandAt(final Location location) {
        return Optional.ofNullable(islands.get(getIslandNumberAt(location)));
    }

    /**
     * Obtains the island with the given number, if it exists.
     *
     * @param number The number.
     * @return The island.
     */
    public Optional<Island> getIsland(final int number) {
        return Optional.ofNullable(islands.get(number));
    }

    /**
     * Obtains or creates an island.
     *
     * @param number The island number.
     * @return The island.
     */
    public Island getOrCreateIsland(final int number) {
        if (number < 0) {
            throw new IllegalArgumentException("invalid island number " + number);
        }

        return islands.computeIfAbsent(number, Island::new);
    }

    /**
     * Obtains the first unowned island number.
     *
     * @return The first unowned island number.
     */
    public int getFirstUnownedIslandNumber() {
        // TODO this does not consider existing unowned, but only non-existing!
        // TODO but what about the spawn island?
        int i = 0;
        for (; i <= islands.size(); ++i) {
            if (!islands.containsKey(i)) {
                return i;
            }
        }
        return i;
    }

    /**
     * Obtains the island number at the given location.
     *
     * @param location The location.
     * @return The island number.
     */
    public int getIslandNumberAt(final Location location) {
        final int gridX = IslandMath.worldToGrid(location.getBlockX(), dimensionX);
        final int gridZ = IslandMath.worldToGrid(location.getBlockZ(), dimensionZ);
        return IslandMath.islandNumber(gridX, gridZ);
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = new HashMap<>();
        result.put("dimX", dimensionX);
        result.put("dimZ", dimensionZ);
        result.put("islands", new ArrayList<>(islands.values()));
        return result;
    }

    /**
     * Checks whether this manager owns the given island.
     *
     * @param island The island.
     * @return {@code true} if the island is owned by this manager.
     */
    public boolean hasIsland(final Island island) {
        return islands.get(island.getNumber()) == island;
    }

    /**
     * Obtains the islands owned by the given user.
     *
     * @param who The user.
     * @return The islands.
     */
    public Collection<Island> getIslandsOf(final UUID who) {
        return islands.values().stream().filter(i -> who.equals(i.getOwner())).collect(Collectors.toList());
    }
}
