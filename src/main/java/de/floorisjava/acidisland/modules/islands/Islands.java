/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.islands;

import de.floorisjava.acidisland.modules.ModuleStatus;
import de.floorisjava.acidisland.modules.islands.model.IslandManager;
import org.bukkit.World;

import java.util.Collection;
import java.util.Optional;

/**
 * API for managing islands.
 */
public interface Islands extends ModuleStatus {

    /**
     * Obtains an island manager for the given world.
     *
     * @param world The world.
     * @return The island manager.
     */
    Optional<IslandManager> getIslandManager(World world);

    /**
     * Obtains the main island world.
     *
     * @return The main island world
     */
    World getMainIslandWorld();

    /**
     * Obtains all island worlds.
     *
     * @return The island worlds.
     */
    Collection<World> getIslandWorlds();
}
