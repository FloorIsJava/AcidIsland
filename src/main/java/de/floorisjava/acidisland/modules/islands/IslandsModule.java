/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.islands;

import de.floorisjava.acidisland.config.ConfigurationProvider;
import de.floorisjava.acidisland.config.ModuleConfigurationProvider;
import de.floorisjava.acidisland.modules.BaseModule;
import de.floorisjava.acidisland.modules.ModuleManager;
import de.floorisjava.acidisland.modules.islands.listener.IslandAnnouncementListener;
import de.floorisjava.acidisland.modules.islands.listener.PavingDisableListener;
import de.floorisjava.acidisland.modules.islands.listener.PortalDisableListener;
import de.floorisjava.acidisland.modules.islands.listener.ProtectionListener;
import de.floorisjava.acidisland.modules.islands.model.Island;
import de.floorisjava.acidisland.modules.islands.model.IslandManager;
import de.floorisjava.acidisland.modules.islands.model.MultiworldIslandManager;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.event.world.WorldUnloadEvent;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Provides islands in worlds.
 */
public class IslandsModule extends BaseModule implements Listener, Islands {

    /**
     * The module configuration.
     */
    private final ConfigurationProvider config;

    /**
     * The multi-world island manager.
     */
    private final MultiworldIslandManager multiworldIslandManager;

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public IslandsModule(final ModuleManager moduleManager) {
        super(moduleManager);

        config = new ModuleConfigurationProvider(moduleManager, "islands");
        multiworldIslandManager = new MultiworldIslandManager(getConfiguration("islands.yml", true));

        addListener(() -> this);
        addListener(() -> new ProtectionListener(logger(), multiworldIslandManager));
        addListener(PortalDisableListener::new);
        addListener(PavingDisableListener::new);
        addListener(() -> new IslandAnnouncementListener(multiworldIslandManager));
    }

    @Override
    protected void startModule() {
        ConfigurationSerialization.registerClass(IslandManager.class);
        ConfigurationSerialization.registerClass(Island.class);

        for (final World world : getModuleManager().getServer().getWorlds()) {
            attemptCreateWorldManager(world);
        }

        addTask(getModuleManager().getServer().getScheduler().scheduleSyncRepeatingTask(getModuleManager(),
                multiworldIslandManager::save, 20L * 60 * 15, 20L * 60 * 15));
    }

    @Override
    protected void stopModule() {
        multiworldIslandManager.save();
        multiworldIslandManager.removeAllIslandManagers();

        ConfigurationSerialization.unregisterClass(Island.class);
        ConfigurationSerialization.unregisterClass(IslandManager.class);
    }

    @EventHandler
    public void onWorldLoad(final WorldLoadEvent event) {
        attemptCreateWorldManager(event.getWorld());
    }

    @EventHandler
    public void onWorldUnload(final WorldUnloadEvent event) {
        multiworldIslandManager.save();
        multiworldIslandManager.removeIslandManager(event.getWorld());
    }

    /**
     * Attempts to create a world island manager for the given world.
     *
     * @param world The world.
     */
    private void attemptCreateWorldManager(final World world) {
        if (isIslandWorld(world)) {
            logger().info("World " + world.getName() + " is configured to have an island manager, creating one");

            final ConfigurationSection worldsSection = config.get().getConfigurationSection("worlds");
            if (worldsSection == null) {
                return;
            }

            final ConfigurationSection worldSection = worldsSection.getConfigurationSection(world.getName());
            if (worldSection != null) {
                multiworldIslandManager.createIslandManager(world, worldSection.getInt("x"), worldSection.getInt("z"));
                logger().info("Created world island manager for world " + world.getName());
            } else {
                logger().severe("Unable to find contained world section for " + world.getName());
            }
        }
    }

    private boolean isIslandWorld(final World world) {
        final ConfigurationSection worldsSection = config.get().getConfigurationSection("worlds");
        return worldsSection != null && worldsSection.contains(world.getName());
    }

    @Override
    public Optional<IslandManager> getIslandManager(final World world) {
        return multiworldIslandManager.getIslandManager(world);
    }

    @Override
    public World getMainIslandWorld() {
        final String worldName = config.get().getString("main-world");
        return worldName != null ? Bukkit.getWorld(worldName) : null;
    }

    @Override
    public Collection<World> getIslandWorlds() {
        return Bukkit.getWorlds().stream().filter(this::isIslandWorld).collect(Collectors.toList());
    }
}
