/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.islands.util;

/**
 * Utilities for island math.
 */
public final class IslandMath {

    /**
     * Converts a grid coordinate to a world coordinate.
     *
     * @param grid      The grid coordinate.
     * @param dimension The dimension factor, i.e. how many world coordinate units should one grid coordinate unit be.
     * @return The world coordinate.
     */
    public static int gridToWorld(final int grid, final int dimension) {
        return grid * dimension;
    }

    /**
     * Converts a world coordinate to a grid coordinate.
     *
     * @param world     The world coordinate.
     * @param dimension The dimension factor, i.e. how many world coordinate units should one grid coordinate unit be.
     * @return The grid coordinate.
     */
    public static int worldToGrid(final int world, final int dimension) {
        return Math.floorDiv(world, dimension);
    }

    /**
     * Determines the island grid x coordinate of the island with the given number.
     *
     * @param n The island number.
     * @return The island grid x coordinate.
     */
    public static int islandX(final int n) {
        final int ring = ring(n);
        final int base = base(ring);
        final int segmentLength = segmentLength(ring);
        final int segment = segment(n, base, segmentLength);

        final int sign = segment > 1 ? -1 : 1;
        switch (segment % 2) {
            case 0:
                return (-ring + segmentOffset(n, ring, segmentLength)) * sign;
            case 1:
                return ring * sign;
        }

        throw new IllegalArgumentException("can't reach");
    }

    /**
     * Determines the island grid z coordinate of the island with the given number.
     *
     * @param n The island number.
     * @return The island grid z coordinate.
     */
    public static int islandZ(final int n) {
        final int ring = ring(n);
        final int base = base(ring);
        final int segmentLength = segmentLength(ring);
        final int segment = segment(n, base, segmentLength);

        final int sign = segment > 1 ? -1 : 1;
        switch (segment % 2) {
            case 0:
                return -ring * sign;
            case 1:
                return (-ring + segmentOffset(n, ring, segmentLength)) * sign;
        }

        throw new IllegalStateException("can't reach");
    }

    /**
     * Determines the island number by the grid coordinates of the island.
     *
     * @param islandX The island grid x coordinate.
     * @param islandZ The island grid z coordinate.
     * @return The island number.
     */
    public static int islandNumber(int islandX, int islandZ) {
        final int ring = ringGrid(islandX, islandZ);
        final int segment = segmentGrid(islandX, islandZ, ring);
        return base(ring)
               + segment * segmentLength(ring)
               + segmentOffsetGrid(islandX, islandZ, ring, segment);
    }

    /**
     * Determines which segment the island number is in.
     * <p>
     * Segments are placed like this in each ring:
     * <pre>
     * 00...01
     * 3     1
     * .     .
     * .     .
     * .     .
     * 3     1
     * 32...22
     * </pre>
     *
     * @param n             The island number.
     * @param base          The base island number, {@code base(ring(n))}.
     * @param segmentLength The segment length, {@code segmentLength(ring(n))}.
     * @return The segment index.
     */
    private static int segment(final int n, final int base, final int segmentLength) {
        return (n - base) / segmentLength;
    }

    /**
     * Determines which segment the island at the given grid coordinates is in.
     *
     * @param x    The x coordinate.
     * @param z    The z coordinate.
     * @param ring The ring number, {@code ringGrid(x, z)}.
     * @return The segment index.
     */
    private static int segmentGrid(final int x, final int z, final int ring) {
        if (x == -ring) {
            return z != -ring ? 3 : 0;
        } else if (x == ring) {
            return z != ring ? 1 : 2;
        } else if (z == -ring) {
            return 0; // X is not at an extremum.
        } else if (z == ring) {
            return 2; // X is not at an extremum.
        }

        throw new IllegalStateException("can't reach");
    }

    /**
     * Determines the offset of the island number in its segment.
     *
     * @param n             The island number.
     * @param ring          The ring number, {@code ring(n)}.
     * @param segmentLength The segment length, {@code segmentLength(ring(n))}.
     * @return The offset in its segment.
     */
    private static int segmentOffset(final int n, final int ring, final int segmentLength) {
        return (n - ring) % segmentLength;
    }

    /**
     * Determines the segment offset of the island at the given grid coordinates.
     *
     * @param x       The x coordinate.
     * @param z       The z coordinate.
     * @param ring    The ring number, {@code ringGrid(x, z)}.
     * @param segment The segment index, {@code segmentGrid(x, z)}.
     * @return The ring number.
     */
    private static int segmentOffsetGrid(final int x, final int z, final int ring, final int segment) {
        final int sign = segment > 1 ? -1 : 1;
        switch (segment % 2) {
            case 0:
                return x * sign + ring;
            case 1:
                return z * sign + ring;
        }

        throw new IllegalStateException("can't reach");
    }

    /**
     * Determines the base island number for the given ring number, that is, the lowest island number in the ring.
     *
     * @param ringNumber The ring number.
     * @return The base island number.
     */
    private static int base(final int ringNumber) {
        return ringNumber == 0 ? 0 : (2 * ringNumber - 1) * (2 * ringNumber - 1);
    }

    /**
     * Determines the length of a segment in the given ring.
     *
     * @param ringNumber The ring number.
     * @return The segment length.
     * @see #segment(int, int, int)
     */
    private static int segmentLength(final int ringNumber) {
        return ringNumber == 0 ? 1 : 2 * ringNumber;
    }

    /**
     * Determines the ring number of the given island number.
     * <p>
     * Rings are placed like this and so on:
     * <pre>
     * 22222
     * 21112
     * 21012
     * 21112
     * 22222
     * </pre>
     *
     * @param n The island number.
     * @return The ring number.
     */
    private static int ring(final int n) {
        return (int) Math.ceil(Math.floor(Math.sqrt(n)) / 2.0);
    }

    /**
     * Determines the ring number of the island at the given grid coordinates.
     *
     * @param x The x coordinate.
     * @param z The z coordinate.
     * @return The ring number.
     */
    private static int ringGrid(final int x, final int z) {
        return Math.max(Math.abs(x), Math.abs(z));
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private IslandMath() {
    }
}
