/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.islands.listener;

import de.floorisjava.acidisland.modules.islands.model.Island;
import de.floorisjava.acidisland.modules.islands.model.IslandManager;
import de.floorisjava.acidisland.modules.islands.model.MultiworldIslandManager;
import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.UUID;

/**
 * Announces island protection domain transitions.
 */
@RequiredArgsConstructor
public class IslandAnnouncementListener implements Listener {

    /**
     * The multi-world island manager.
     */
    private final MultiworldIslandManager manager;

    @EventHandler
    public void onPlayerMove(final PlayerMoveEvent event) {
        final World world = event.getFrom().getWorld();
        if (event.getTo() == null || world != event.getTo().getWorld() || world == null) {
            return;
        }

        final IslandManager islandManager = manager.getIslandManager(world).orElse(null);
        if (islandManager == null) {
            return;
        }

        final Island islandA = islandManager.getIslandAt(event.getFrom()).orElse(null);
        final Island islandB = islandManager.getIslandAt(event.getTo()).orElse(null);
        if (islandA == islandB) {
            return;
        }

        final String alert;
        if (islandB == null) {
            alert = "§eTerra Incognita";
        } else {
            final UUID ownerB = islandB.getOwner();
            if (islandA != null) {
                final UUID ownerA = islandA.getOwner();
                if (ownerA != null && ownerA.equals(ownerB)) {
                    return;
                }
            }

            if (ownerB != null) {
                final OfflinePlayer playerB = Bukkit.getOfflinePlayer(ownerB);
                alert = "§e" + playerB.getName() + "'s Island";
            } else {
                alert = "§eInternational Waters";
            }
        }
        event.getPlayer().sendTitle(alert, "", 10, 70, 20);
    }
}
