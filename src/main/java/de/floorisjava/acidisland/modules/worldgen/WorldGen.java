/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.worldgen;

import de.floorisjava.acidisland.modules.ModuleStatus;
import org.bukkit.generator.ChunkGenerator;

/**
 * API for world generation.
 */
public interface WorldGen extends ModuleStatus {

    /**
     * Obtains a chunk generator for the given generator id.
     *
     * @param id The id.
     * @return The chunk generator.
     */
    ChunkGenerator getGeneratorForId(String id);
}
