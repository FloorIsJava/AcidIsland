/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.acid;

import de.floorisjava.acidisland.config.ConfigurationProvider;
import de.floorisjava.acidisland.config.ModuleConfigurationProvider;
import de.floorisjava.acidisland.modules.BaseModule;
import de.floorisjava.acidisland.modules.ModuleManager;
import de.floorisjava.acidisland.modules.islands.Islands;
import de.floorisjava.acidisland.modules.quests.Quests;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Boat;
import org.bukkit.entity.Entity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

/**
 * Makes water behave like acid.
 */
public class AcidModule extends BaseModule {

    /**
     * The module configuration.
     */
    private final ConfigurationProvider config;

    /**
     * Maps player UUIDs to cooldown expiration times.
     */
    private final Map<UUID, Long> cooldownMap = new HashMap<>();

    /**
     * The quest module API.
     */
    private Quests quests;

    /**
     * The island module API.
     */
    private Islands islands;

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public AcidModule(final ModuleManager moduleManager) {
        super(moduleManager);

        config = new ModuleConfigurationProvider(moduleManager, "acid");
    }

    @Override
    public void startModule() {
        quests = getModuleManager().get(Quests.class);
        islands = Objects.requireNonNull(getModuleManager().get(Islands.class));

        final long acidCheckPeriod = config.get().getLong("damage.check-period");
        addTask(getModuleManager().getServer().getScheduler().scheduleSyncRepeatingTask(getModuleManager(),
                this::checkEntities, acidCheckPeriod, acidCheckPeriod));
    }

    @Override
    protected void stopModule() {
        cooldownMap.clear();
    }

    /**
     * Performs an acid check for all living entities.
     */
    private void checkEntities() {
        islands.getIslandWorlds().stream()
                .flatMap(w -> w.getLivingEntities().stream())
                .filter(this::isNotAcidImmune)
                .forEach(this::checkEntity);
    }

    /**
     * Checks an entity for whether acid effects need to be applied.
     *
     * @param entity The entity.
     */
    private void checkEntity(final LivingEntity entity) {
        if (isEntityOnCooldown(entity)) {
            return;
        }

        final Block feetBlock = entity.getLocation().getBlock();

        final boolean acidAtFeet = feetBlock.getType() == Material.WATER;
        final boolean inAcid;
        if (entity.getHeight() > 1) {
            final Block headBlock = feetBlock.getRelative(BlockFace.UP);
            final boolean acidAtHead = headBlock.getType() == Material.WATER;
            inAcid = acidAtFeet || acidAtHead;
        } else {
            inAcid = acidAtFeet;
        }

        if (inAcid && !(entity.getVehicle() instanceof Boat)) {
            applyAcidEffects(entity, config.get().getDouble("damage.block"), true);
        } else if (entity.getWorld().hasStorm()) {
            if (entity.getWorld().getHighestBlockYAt(entity.getLocation()) < entity.getEyeLocation().getBlockY()) {
                if (entity instanceof Player && quests != null) {
                    quests.playerReceivedAcidRainDamage((Player) entity);
                }
                applyAcidEffects(entity, config.get().getDouble("damage.rain"), false);
            }
        }
    }

    /**
     * Applies acid effects to the entity.
     *
     * @param entity        The entity to apply effects to.
     * @param damage        The damage to deal.
     * @param potionEffects Whether to apply potion effects or not.
     */
    private void applyAcidEffects(final LivingEntity entity, final double damage, final boolean potionEffects) {
        if (entity instanceof HumanEntity) {
            switch (((HumanEntity) entity).getGameMode()) {
                case CREATIVE:
                case SPECTATOR:
                    return;
                default:
                    break;
            }
        }

        if (entity.hasPotionEffect(PotionEffectType.WATER_BREATHING) || entity.isDead()) {
            return;
        }

        putEntityOnCooldown(entity, config.get().getLong("damage.cooldown"));
        entity.damage(damage);

        entity.getWorld().playSound(entity.getLocation(), Sound.ENTITY_GENERIC_BURN, 1.0f, 0.75f);
        if (potionEffects) {
            final int debuffDuration = config.get().getInt("effects.duration");
            entity.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, debuffDuration, 0, true, false));
            entity.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, debuffDuration, 0, true, false));
            entity.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, debuffDuration, 0, true, false));
            entity.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, debuffDuration, 0, true, false));
        }
    }

    /**
     * Checks whether the entity is on cooldown.
     *
     * @param entity The entity.
     * @return {@code true} if and only if the entity is on cooldown.
     */
    private boolean isEntityOnCooldown(final Entity entity) {
        return cooldownMap.getOrDefault(entity.getUniqueId(), Long.MIN_VALUE) > System.currentTimeMillis();
    }

    /**
     * Puts an entity on cooldown.
     *
     * @param entity       The entity.
     * @param cooldownTime The cooldown, in milliseconds.
     */
    private void putEntityOnCooldown(final Entity entity, final long cooldownTime) {
        cooldownMap.put(entity.getUniqueId(), System.currentTimeMillis() + cooldownTime);
    }

    /**
     * Checks whether an entity is acid immune or not.
     *
     * @param entity The entity.
     * @return {@code true} if and only if the entity is not acid immune.
     */
    private boolean isNotAcidImmune(final LivingEntity entity) {
        switch (entity.getType()) {
            case ELDER_GUARDIAN:
            case ARMOR_STAND:
            case VEX:
            case GUARDIAN:
            case TURTLE:
            case COD:
            case SALMON:
            case PUFFERFISH:
            case TROPICAL_FISH:
            case DROWNED:
            case DOLPHIN:
            case SQUID:
                return false;
            default:
                return true;
        }
    }
}
