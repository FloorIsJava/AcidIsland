/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.command;

import de.floorisjava.acidisland.modules.BaseModule;
import de.floorisjava.acidisland.modules.ModuleManager;
import de.floorisjava.acidisland.modules.command.util.CommandsDetail;

/**
 * Provides the commands of the plugin.
 */
public class CommandModule extends BaseModule implements CommandsDetail {

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public CommandModule(final ModuleManager moduleManager) {
        super(moduleManager);
    }

    @Override
    protected void startModule() {
        getModuleManager().getCommand("acidisland").setExecutor(new AcidIslandCommand(getModuleManager()));
    }

    @Override
    protected void stopModule() {
        getModuleManager().getCommand("acidisland").setExecutor(null);
    }

    @Override
    public void scheduleTask(final Runnable task, final long delay) {
        addTask(getModuleManager().getServer().getScheduler().scheduleSyncDelayedTask(getModuleManager(), task, delay));
    }
}
