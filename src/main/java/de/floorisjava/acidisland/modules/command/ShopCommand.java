/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.command;

import de.floorisjava.acidisland.command.LeafCommand;
import de.floorisjava.acidisland.modules.ModuleManager;
import de.floorisjava.acidisland.modules.command.util.CommandHelper;
import de.floorisjava.acidisland.modules.islands.Islands;
import de.floorisjava.acidisland.modules.shop.Shop;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /acidisland shop
 */
public class ShopCommand extends LeafCommand {

    /**
     * The islands API.
     */
    private final Islands islands;

    /**
     * The shop API.
     */
    private final Shop shop;

    /**
     * Constructor.
     *
     * @param manager The module manager.
     */
    public ShopCommand(final ModuleManager manager) {
        super("§b/acidisland shop - Opens the island shop", "acidisland.shop");
        islands = manager.get(Islands.class);
        shop = manager.get(Shop.class);
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        if (!CommandHelper.checkDependency(sender, islands, Islands.class)
            || !CommandHelper.checkDependency(sender, shop, Shop.class)) {
            return;
        }

        if (!CommandHelper.ensurePlayer(sender)) {
            return;
        }
        final Player player = (Player) sender;

        // Allow the shop to be only used in island worlds, for cases of per-world inventories.
        if (!islands.getIslandWorlds().contains(player.getWorld())) {
            sender.sendMessage("§cYou are not in an island world.");
            return;
        }
        shop.openShop(player);
    }
}
