/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.command;

import de.floorisjava.acidisland.command.LeafCommand;
import de.floorisjava.acidisland.modules.ModuleManager;
import de.floorisjava.acidisland.modules.command.util.CommandHelper;
import de.floorisjava.acidisland.modules.command.util.CommandsDetail;
import de.floorisjava.acidisland.modules.islands.Islands;
import de.floorisjava.acidisland.modules.islands.model.Island;
import de.floorisjava.acidisland.modules.islands.model.IslandManager;
import de.floorisjava.acidisland.util.TeleportTask;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collection;

/**
 * /acidisland home
 */
public class HomeCommand extends LeafCommand {

    /**
     * The islands API.
     */
    private final Islands islands;

    /**
     * The commands detail API.
     */
    private final CommandsDetail commandsDetail;

    /**
     * Constructor.
     *
     * @param manager The module manager.
     */
    public HomeCommand(final ModuleManager manager) {
        super("§b/acidisland home - Teleports you to your island", "acidisland.home");
        islands = manager.get(Islands.class);
        commandsDetail = manager.get(CommandsDetail.class);
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        if (!CommandHelper.checkDependency(sender, islands, Islands.class)
            || !CommandHelper.checkDependency(sender, commandsDetail, CommandsDetail.class)) {
            return;
        }

        if (!CommandHelper.ensurePlayer(sender)) {
            return;
        }
        final Player player = (Player) sender;

        final IslandManager islandManager = CommandHelper.getMainIslandManager(islands, player);
        if (islandManager == null) {
            return;
        }

        final Collection<Island> islands = islandManager.getIslandsOf(player.getUniqueId());
        if (islands.isEmpty()) {
            sender.sendMessage("§cYou do not own an island.");
            return;
        }

        // TODO multi-islands?
        final Island island = islands.stream().findAny().get();
        sender.sendMessage("§eTeleporting you to your home in 3 seconds... Don't move!");

        final Location spawnPoint = island.getSpawnPoint();
        if (spawnPoint == null) {
            sender.sendMessage("§cYour spawnpoint is not present... Contact an administrator!");
            return;
        }
        final TeleportTask task = new TeleportTask(player.getLocation(), player, spawnPoint);
        commandsDetail.scheduleTask(task, 20L * 3);
    }
}
