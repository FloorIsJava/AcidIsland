/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.command.debug;

import de.floorisjava.acidisland.command.LeafCommand;
import de.floorisjava.acidisland.modules.ModuleManager;
import de.floorisjava.acidisland.modules.command.util.CommandHelper;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * /acidisland debug item
 */
public class ItemCommand extends LeafCommand {

    /**
     * The logger.
     */
    private final Logger logger;

    /**
     * Constructor.
     *
     * @param manager The module manager.
     */
    public ItemCommand(final ModuleManager manager) {
        super("§b/acidisland debug item - Display item information", "acidisland.debug.item");
        logger = manager.getLogger();
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        if (!CommandHelper.ensurePlayer(sender)) {
            return;
        }
        final Player player = (Player) sender;

        final ItemStack mainItem = player.getInventory().getItemInMainHand();

        final Map<String, Object> data = mainItem.serialize();
        debugObject(data);
        player.sendMessage("dumped object to logger");
    }

    /**
     * Prints the given object to the logger.
     *
     * @param obj The object.
     */
    private void debugObject(final Object obj) {
        logger.info("{");
        if (obj instanceof Map<?, ?>) {
            for (final Map.Entry<?, ?> entry : ((Map<?, ?>) obj).entrySet()) {
                logger.info("Key is " + entry.getKey());
                debugObject(entry.getValue());
            }
        } else if (obj instanceof List<?>) {
            for (final Object entry : (List<?>) obj) {
                debugObject(entry);
            }
        } else if (obj instanceof ConfigurationSerializable) {
            debugObject(((ConfigurationSerializable) obj).serialize());
        } else {
            logger.info(String.valueOf(obj));
        }
        logger.info("}");
    }
}
