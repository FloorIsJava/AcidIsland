/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.command;

import de.floorisjava.acidisland.command.LeafCommand;
import de.floorisjava.acidisland.modules.ModuleManager;
import de.floorisjava.acidisland.modules.command.util.CommandHelper;
import de.floorisjava.acidisland.modules.islands.Islands;
import de.floorisjava.acidisland.modules.islands.model.Island;
import de.floorisjava.acidisland.modules.islands.model.IslandManager;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Optional;

/**
 * /acidisland sethome
 */
public class SethomeCommand extends LeafCommand {

    /**
     * The islands API.
     */
    private final Islands islands;

    /**
     * Constructor.
     *
     * @param manager The module manager.
     */
    public SethomeCommand(final ModuleManager manager) {
        super("§b/acidisland sethome - Sets the spawn point of your island", "acidisland.sethome");
        islands = manager.get(Islands.class);
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        if (!CommandHelper.checkDependency(sender, islands, Islands.class)) {
            return;
        }

        if (!CommandHelper.ensurePlayer(sender)) {
            return;
        }
        final Player player = (Player) sender;

        if (player.getWorld() != islands.getMainIslandWorld()) {
            sender.sendMessage("§cYou are not in the main island world!");
            return;
        }

        final IslandManager islandManager = CommandHelper.getMainIslandManager(islands, player);
        if (islandManager == null) {
            return;
        }

        final Optional<Island> islandOpt = islandManager.getIslandAt(player.getLocation());
        if (!islandOpt.isPresent()) {
            sender.sendMessage("§cThere is no island here!");
            return;
        }
        final Island island = islandOpt.get();

        if (!player.isOp() && !player.getUniqueId().equals(island.getOwner())) {
            sender.sendMessage("§cThis is not your island!");
            return;
        }

        island.setSpawnPoint(player.getLocation());
        player.sendMessage("§aIsland spawn point set.");
    }
}
