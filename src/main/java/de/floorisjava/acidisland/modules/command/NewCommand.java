/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.command;

import de.floorisjava.acidisland.command.LeafCommand;
import de.floorisjava.acidisland.modules.ModuleManager;
import de.floorisjava.acidisland.modules.command.util.CommandHelper;
import de.floorisjava.acidisland.modules.islands.Islands;
import de.floorisjava.acidisland.modules.islands.model.Island;
import de.floorisjava.acidisland.modules.islands.model.IslandManager;
import de.floorisjava.acidisland.modules.islands.populator.ParadisePopulator;
import de.floorisjava.acidisland.modules.quests.Quests;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /acidisland new
 */
public class NewCommand extends LeafCommand {

    /**
     * The islands API.
     */
    private final Islands islands;

    /**
     * The quests API.
     */
    private final Quests quests;

    /**
     * Constructor.
     *
     * @param manager The module manager.
     */
    public NewCommand(final ModuleManager manager) {
        super("§b/acidisland new - Creates a new island", "acidisland.new");
        islands = manager.get(Islands.class);
        quests = manager.get(Quests.class);
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        if (!CommandHelper.checkDependency(sender, islands, Islands.class)
            || !CommandHelper.checkDependency(sender, quests, Quests.class)) {
            return;
        }

        if (!CommandHelper.ensurePlayer(sender)) {
            return;
        }
        final Player player = (Player) sender;

        if (player.getWorld() != islands.getMainIslandWorld()) {
            sender.sendMessage("§cYou are not in the main island world.");
            return;
        }

        final IslandManager islandManager = CommandHelper.getMainIslandManager(islands, player);
        if (islandManager == null) {
            return;
        }

        if (!islandManager.getIslandsOf(player.getUniqueId()).isEmpty()) {
            sender.sendMessage("§cYou already own an island.");
            return;
        }

        final Island island = islandManager.getOrCreateIsland(islandManager.getFirstUnownedIslandNumber());
        island.populate(player.getWorld(), islandManager, new ParadisePopulator());

        sender.sendMessage("§aIsland #" + island.getNumber() + " has been created.");
        quests.playerJoinedIsland(player);
        island.setOwner(player.getUniqueId());
        player.teleport(island.getSpawnPoint());
    }
}
