/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.command.util;

import de.floorisjava.acidisland.modules.ModuleStatus;
import de.floorisjava.acidisland.modules.islands.Islands;
import de.floorisjava.acidisland.modules.islands.model.IslandManager;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Optional;

/**
 * Provides utilities for commands.
 */
public class CommandHelper {

    /**
     * Ensures that the given command sender is a player.
     *
     * @param sender The command sender.
     * @return {@code true} if the command sender is a player.
     */
    public static boolean ensurePlayer(final CommandSender sender) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("§cThis action requires a player.");
            return false;
        }
        return true;
    }

    /**
     * Checks that a dependency is existing, and started, if applicable.
     *
     * @param issuer     The issuer for the action, who will be notified on failure.
     * @param dependency The dependency.
     * @param clazz      The dependency class.
     * @param <T>        The dependency type.
     * @return {@code true} if the dependency is existing (and started).
     */
    public static <T> boolean checkDependency(final CommandSender issuer, final T dependency, final Class<T> clazz) {
        if (dependency == null) {
            issuer.sendMessage("§cModule dependency not fulfilled: " + clazz.getSimpleName());
            return false;
        } else {
            if (dependency instanceof ModuleStatus) {
                if (!((ModuleStatus) dependency).isStarted()) {
                    issuer.sendMessage("§cModule dependency not started: " + clazz.getSimpleName());
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Ensures that the given world is an islands world and obtains the island manager.
     *
     * @param islands The islands.
     * @param issuer  The issuer for the action, who will be notified on failure.
     * @param world   The world.
     * @return The island manager, or null.
     */
    public static IslandManager ensureIslandWorld(final Islands islands, final CommandSender issuer,
                                                  final World world) {
        final Optional<IslandManager> islandManagerOpt = islands.getIslandManager(world);
        if (!islandManagerOpt.isPresent()) {
            issuer.sendMessage("§cThis is not an island world.");
            return null;
        }
        return islandManagerOpt.get();
    }

    /**
     * Ensures that the given world is an islands world and obtains the island manager.
     *
     * @param islands The islands.
     * @param issuer  The issuer for the action, who will be notified on failure.
     * @return The island manager, or null.
     */
    public static IslandManager getMainIslandManager(final Islands islands, final CommandSender issuer) {
        final World world = islands.getMainIslandWorld();
        if (world != null) {
            final Optional<IslandManager> islandManagerOpt = islands.getIslandManager(islands.getMainIslandWorld());
            if (islandManagerOpt.isPresent()) {
                return islandManagerOpt.get();
            }
        }
        issuer.sendMessage("§cError: No main island world!");
        return null;
    }
}
