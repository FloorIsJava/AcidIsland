/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.command.debug;

import de.floorisjava.acidisland.command.LeafCommand;
import de.floorisjava.acidisland.modules.ModuleManager;
import de.floorisjava.acidisland.modules.command.util.CommandHelper;
import de.floorisjava.acidisland.modules.islands.Islands;
import de.floorisjava.acidisland.modules.islands.model.Island;
import de.floorisjava.acidisland.modules.islands.model.IslandManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * /acidisland debug island
 */
public class IslandCommand extends LeafCommand {

    /**
     * The islands API.
     */
    private final Islands islands;

    /**
     * Constructor.
     *
     * @param manager The module manager.
     */
    public IslandCommand(final ModuleManager manager) {
        super("§b/acidisland debug island - Display island information", "acidisland.debug.island");
        islands = manager.get(Islands.class);
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        if (!CommandHelper.checkDependency(sender, islands, Islands.class)) {
            return;
        }

        if (!CommandHelper.ensurePlayer(sender)) {
            return;
        }
        final Player player = (Player) sender;

        final IslandManager islandManager = CommandHelper.ensureIslandWorld(islands, player, player.getWorld());
        if (islandManager == null) {
            return;
        }

        final Location location = player.getLocation();
        final Optional<Island> islandOpt = islandManager.getIslandAt(location);
        if (islandOpt.isPresent()) {
            final Island island = islandOpt.get();
            sender.sendMessage("Island present, number is " + island.getNumber());

            final UUID owner = island.getOwner();
            if (owner == null) {
                sender.sendMessage("Island is unowned");
            } else {
                final OfflinePlayer ownerPlayer = Bukkit.getOfflinePlayer(owner);
                sender.sendMessage("Island Owner: " + ownerPlayer.getName());
            }

            sender.sendMessage("Island members: " + island.getMembers().stream()
                    .map(Bukkit::getOfflinePlayer)
                    .map(OfflinePlayer::getName)
                    .collect(Collectors.joining(", ")));
        } else {
            sender.sendMessage("Island not present, number would be " + islandManager.getIslandNumberAt(location));
        }

    }
}
