/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.command.debug;

import de.floorisjava.acidisland.command.TreeCommand;
import de.floorisjava.acidisland.modules.ModuleManager;

/**
 * /acidisland debug
 */
public class DebugCommand extends TreeCommand {

    /**
     * Constructor.
     *
     * @param manager The module manager.
     */
    public DebugCommand(final ModuleManager manager) {
        super(null); // null to prevent the command from showing up in the help
        addSubCommand("island", new IslandCommand(manager));
        addSubCommand("item", new ItemCommand(manager));
        addSubCommand("populate", new PopulateCommand(manager));
    }
}
