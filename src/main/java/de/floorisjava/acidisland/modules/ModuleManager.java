/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules;

import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.Plugin;

import java.util.Locale;

/**
 * Represents a module manager.
 */
public interface ModuleManager extends Plugin {

    /**
     * Obtains the module implementing the given interface. If multiple modules implement the given interface, this
     * method returns any of them. Returns {@code null} if there is no such module.
     *
     * @param clazz The interface.
     * @param <T>   The interface type.
     * @return The implementing module.
     */
    <T> T get(Class<T> clazz);

    /**
     * This should be in {@link Plugin}, but alas, it is not.
     *
     * @see org.bukkit.plugin.java.JavaPlugin#getCommand(String)
     */
    default PluginCommand getCommand(final String name) {
        final String alias = name.toLowerCase(Locale.ENGLISH);
        PluginCommand command = getServer().getPluginCommand(alias);

        if (command == null || command.getPlugin() != this) {
            command = getServer().getPluginCommand(getName().toLowerCase(Locale.ENGLISH) + ":" + alias);
        }

        if (command != null && command.getPlugin() == this) {
            return command;
        } else {
            return null;
        }
    }
}
