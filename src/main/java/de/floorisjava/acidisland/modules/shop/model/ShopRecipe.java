/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.shop.model;

import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.advancement.Advancement;
import org.bukkit.advancement.AdvancementProgress;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.MerchantRecipe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SerializableAs("de.floorisjava.acidisland.serial.ShopRecipe")
public class ShopRecipe implements ConfigurationSerializable {

    /**
     * The requirement advancement which has to be done before the recipe is accessible.
     */
    private final NamespacedKey requirementKey;

    /**
     * The ingredients.
     */
    private final List<ItemStack> ingredients = new ArrayList<>();

    /**
     * The result.
     */
    private final ItemStack result;

    /**
     * Deserialization constructor.
     *
     * @param serial The serialized object.
     */
    public ShopRecipe(final Map<String, Object> serial) {
        final String serialRequirement = (String) serial.get("requirement");
        if (serialRequirement != null && serialRequirement.indexOf(':') != -1) {
            final String[] split = serialRequirement.split(":");
            //noinspection deprecation: Serialization of arbitrary key.
            requirementKey = new NamespacedKey(split[0], split[1]);
        } else {
            requirementKey = null;
        }

        final List<?> ingredients = (List<?>) serial.get("ingredients");
        ingredients.stream()
                .filter(ItemStack.class::isInstance)
                .map(ItemStack.class::cast)
                .forEach(this.ingredients::add);
        if (this.ingredients.isEmpty()) {
            throw new IllegalArgumentException("no ingredients for recipe");
        }

        result = (ItemStack) serial.get("result");
    }

    /**
     * Creates a {@link MerchantRecipe} for this shop recipe.
     *
     * @return The merchant recipe.
     */
    public MerchantRecipe getRecipe() {
        final MerchantRecipe recipe = new MerchantRecipe(result, 0, Integer.MAX_VALUE, false, 0, 0.0f);
        ingredients.forEach(recipe::addIngredient);
        return recipe;
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = new HashMap<>();
        if (requirementKey != null) {
            result.put("requirement", requirementKey.getNamespace() + ":" + requirementKey.getKey());
        }
        result.put("ingredients", ingredients);
        result.put("result", this.result);
        return result;
    }

    /**
     * Checks whether this recipe can be used by the given player.
     *
     * @param target The player.
     * @return {@code true} if the recipe can be used by the given player.
     */
    public boolean canBeUsedBy(final Player target) {
        if (requirementKey == null) {
            return true;
        }

        final Advancement advancement = Bukkit.getAdvancement(requirementKey);
        if (advancement != null) {
            final AdvancementProgress progress = target.getAdvancementProgress(advancement);
            return progress.isDone();
        }
        return false;
    }
}
