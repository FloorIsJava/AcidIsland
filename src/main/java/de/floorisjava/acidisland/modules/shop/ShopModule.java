/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.shop;

import de.floorisjava.acidisland.modules.BaseModule;
import de.floorisjava.acidisland.modules.ModuleManager;
import de.floorisjava.acidisland.modules.shop.model.ShopManager;
import de.floorisjava.acidisland.modules.shop.model.ShopRecipe;
import org.bukkit.Bukkit;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Merchant;

/**
 * Provides a trade shop.
 */
public class ShopModule extends BaseModule implements Shop {

    /**
     * The shop manager.
     */
    private ShopManager shopManager;

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public ShopModule(final ModuleManager moduleManager) {
        super(moduleManager);

        copyDefaultConfiguration("shop.yml");
    }

    @Override
    protected void startModule() {
        ConfigurationSerialization.registerClass(ShopManager.class);
        ConfigurationSerialization.registerClass(ShopRecipe.class);
        shopManager = getConfiguration("shop.yml", false).getConfig().getSerializable("shop", ShopManager.class);
    }

    @Override
    protected void stopModule() {
        shopManager = null;
        ConfigurationSerialization.unregisterClass(ShopRecipe.class);
        ConfigurationSerialization.unregisterClass(ShopManager.class);
    }

    @Override
    public void openShop(final Player target) {
        final Merchant merchant = Bukkit.createMerchant("§cShop");
        merchant.setRecipes(shopManager.getRecipesForPlayer(target));
        target.openMerchant(merchant, false);
    }
}
