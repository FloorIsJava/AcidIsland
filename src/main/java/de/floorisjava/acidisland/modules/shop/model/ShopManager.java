/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.modules.shop.model;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Player;
import org.bukkit.inventory.MerchantRecipe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Manages {@link ShopRecipe}s.
 */
@SerializableAs("de.floorisjava.acidisland.serial.ShopManager")
public class ShopManager implements ConfigurationSerializable {

    /**
     * The registered shop recipes.
     */
    private final List<ShopRecipe> recipes = new ArrayList<>();

    /**
     * Deserialization constructor.
     *
     * @param serial The serialized object.
     */
    public ShopManager(final Map<String, Object> serial) {
        final List<?> serialList = (List<?>) serial.get("recipes");
        serialList.stream()
                .filter(ShopRecipe.class::isInstance)
                .map(ShopRecipe.class::cast)
                .forEach(recipes::add);
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = new HashMap<>();
        result.put("recipes", recipes);
        return result;
    }

    /**
     * Obtains the recipes for the given player.
     *
     * @param target The player.
     * @return The recipes.
     */
    public List<MerchantRecipe> getRecipesForPlayer(final Player target) {
        return recipes.stream()
                .filter(r -> r.canBeUsedBy(target))
                .map(ShopRecipe::getRecipe)
                .collect(Collectors.toList());
    }
}
