/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

/**
 * Represents a node in a command tree.
 */
public class TreeCommand implements CommandExecutor {

    /**
     * The subcommands of this tree command.
     */
    private final Map<String, TreeCommand> subCommands = new TreeMap<>();

    /**
     * The help line.
     */
    private final String helpLine;

    /**
     * Constructor.
     *
     * @param helpLine The help line.
     */
    public TreeCommand(final String helpLine) {
        this.helpLine = helpLine;
    }

    /**
     * Adds a subcommand.
     *
     * @param key     The key of the subcommand.
     * @param command The command which is stored for this key.
     */
    protected void addSubCommand(final String key, final TreeCommand command) {
        subCommands.put(key, command);
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label,
                             final String[] args) {
        // No arguments: Help.
        if (args.length == 0) {
            for (final TreeCommand tc : subCommands.values()) {
                if (tc.helpLine != null) {
                    sender.sendMessage(tc.helpLine);
                }
            }
            return true;
        }

        // Fetch the subcommand.
        final String subCommand = args[0];
        final TreeCommand treeCommand = subCommands.get(subCommand);

        // Execute the subcommand if possible.
        if (treeCommand != null) {
            treeCommand.onCommand(sender, command, label, Arrays.copyOfRange(args, 1, args.length));
        } else {
            sender.sendMessage("Command not found!");
        }

        return true;
    }
}
