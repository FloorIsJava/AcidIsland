/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland;

import de.floorisjava.acidisland.modules.Module;
import de.floorisjava.acidisland.modules.ModuleManager;
import de.floorisjava.acidisland.modules.acid.AcidModule;
import de.floorisjava.acidisland.modules.command.CommandModule;
import de.floorisjava.acidisland.modules.deathpenalty.DeathPenaltyModule;
import de.floorisjava.acidisland.modules.gameplay.GameplayModule;
import de.floorisjava.acidisland.modules.islands.IslandsModule;
import de.floorisjava.acidisland.modules.quests.QuestModule;
import de.floorisjava.acidisland.modules.shop.ShopModule;
import de.floorisjava.acidisland.modules.worldgen.WorldGen;
import de.floorisjava.acidisland.modules.worldgen.WorldGeneratorModule;
import de.floorisjava.acidisland.util.EmergencyListener;
import de.floorisjava.acidisland.util.EntryPoint;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

/**
 * Plugin handle.
 */
@EntryPoint
public class AcidIsland extends JavaPlugin implements ModuleManager {

    /**
     * The list of enabled modules.
     */
    private final List<Module> modules = new ArrayList<>();

    @Override
    public void onEnable() {
        tryExecute(() -> {
            initializeConfig();

            getLogger().info("Initializing all modules");

            // Create modules here.
            modules.add(new AcidModule(this));
            modules.add(new CommandModule(this));
            modules.add(new DeathPenaltyModule(this));
            modules.add(new GameplayModule(this));
            modules.add(new IslandsModule(this));
            modules.add(new ShopModule(this));
            modules.add(new QuestModule(this));
            modules.add(new WorldGeneratorModule(this));

            getLogger().info("Initialized all modules");

            getLogger().info("Starting all modules");
            modules.forEach(m -> tryExecute(m::start));
            getLogger().info("Started all modules");
        });
    }

    @Override
    public void onDisable() {
        getLogger().info("Stopping all modules");
        modules.forEach(m -> tryExecute(m::stop));
        modules.clear();
        getLogger().info("Stopped all modules");
    }

    /**
     * Initializes the config.
     */
    private void initializeConfig() {
        // First, save the default config, then (re)load it immediately, then save missing defaults to disk, then reload.
        saveDefaultConfig();
        reloadConfig();
        getConfig().options().copyDefaults(true);
        saveConfig();
        reloadConfig();
    }

    @Override
    public ChunkGenerator getDefaultWorldGenerator(final String worldName, final String id) {
        final WorldGen worldGen = get(WorldGen.class);
        if (worldGen != null && worldGen.isStarted()) {
            return worldGen.getGeneratorForId(id);
        } else {
            throw new IllegalStateException("Module dependency WorldGen not fulfilled");
        }
    }

    /**
     * Trys to execute the given runnable, installing an {@link EmergencyListener} if an exception is thrown.
     *
     * @param runnable The runnable.
     */
    private void tryExecute(final Runnable runnable) {
        try {
            runnable.run();
        } catch (final Exception ex) {
            getLogger().severe("Uncaught exception! Installing EmergencyListener...");
            ex.printStackTrace();
            getServer().getPluginManager().registerEvents(new EmergencyListener(), this);
        }
    }

    @Override
    public <T> T get(final Class<T> clazz) {
        return modules.stream().filter(clazz::isInstance).map(clazz::cast).findAny().orElse(null);
    }
}
