/*
 *     An Acid Island Clone
 *     Copyright (C) 2020 FloorIsJava
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.acidisland.util;

import lombok.RequiredArgsConstructor;
import org.bukkit.Location;
import org.bukkit.entity.Entity;

/**
 * A task that performs a teleport with warmup during which a player may not move.
 */
@RequiredArgsConstructor
public class TeleportTask implements Runnable {

    /**
     * The origin location of the teleport.
     */
    private final Location origin;

    /**
     * The teleportation target.
     */
    private final Entity target;

    /**
     * The teleport destination.
     */
    private final Location destination;

    @Override
    public void run() {
        if (target.getWorld() == origin.getWorld()) {
            if (target.getLocation().distanceSquared(origin) <= 0.5 * 0.5) {
                target.teleport(destination);
                return;
            }
        }

        target.sendMessage("§cTeleportation cancelled due to movement!");
    }
}
